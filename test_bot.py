import telebot
from telebot import types
from db import get_current, append_chat_id, get_ids, update_current
from level3_text import ret

API_TOKEN = '1146547926:AAH8q4Ip7ASlnB4jWbP5--u9lYvXdoMDDY4'

welcome_msg = '''Приветствуем на GET Business Festival 2020 🔥! Мы уверены, что полученные сегодня знания обеспечат рост Вашего бизнеса в 2020 году! .

    Сегодня вас ожидает:
    🎯70 выступлений спикеров 
    🎯4 потока: Маркетинг | Digital, Точки роста, PR | HR
    🎯зона бесплатных бизнес-консультаций
    🎯нетворкинг c 2000+ участников
    🎯сессия “вопрос-ответ” с Василием Хмельницким
    а главное: 875 лет совокупного опыта от бизнес-практиков!

    ⏰ Время проведения фестиваля с 9:00 до 18:50. Спикеры выступают без перерывов нон-стоп.
    ⏳У каждого спикера есть 20 минут + 5 минут на вопросы от участников

    На территории фестиваля действует 🍕фудкорт и 📣экспо-зона

    Сориентироваться в залах вам всегда поможет наша 👫Help team команда - ищите парней и девушек в черных футболках Get Team

                '''

welcome_msg2 = '''Полная программа в .pdf'''

welcome_msg3 = '''План размещения потоков и зон фестиваля'''


bot = telebot.TeleBot(API_TOKEN)

msg_l1 = ['Программа',
        'Кто сейчас выступает?',
        'Получить всю программу',
        'Про организатора']

level1 = types.ReplyKeyboardMarkup(row_width=2)
itembtn1 = types.KeyboardButton(    msg_l1[0]   )
itembtn2 = types.KeyboardButton(    msg_l1[1]   )
itembtn3 = types.KeyboardButton(    msg_l1[2]   )
itembtn4 = types.KeyboardButton(    msg_l1[3]   )

level1.add(itembtn1, itembtn2, itembtn3, itembtn4)


msg_l2 = ['Главная сцена',
          'Маркетинг|Digital',
          'Точки роста',
          'PR|HR',
          'Финансы', 'Вернуться назад']

level2 = types.ReplyKeyboardMarkup(row_width=2)
itembtn11 = types.KeyboardButton(    msg_l2[0]    )
itembtn21 = types.KeyboardButton(    msg_l2[1]   )
itembtn31 = types.KeyboardButton(    msg_l2[2]   )
itembtn41 = types.KeyboardButton(    msg_l2[3]   )
itembtn51 = types.KeyboardButton(    msg_l2[4]   )
itembtn61 = types.KeyboardButton(    msg_l2[5]   )
level2.add(itembtn11, itembtn21, itembtn31, itembtn41, itembtn51, itembtn61)

# Handle '/start' and '/help'
@bot.message_handler(commands=['help', 'start', 'sendall7218', 'update7218'])
def send_welcome(message):

    if message.text == '/sendall7218':
        bot.send_message(message.chat.id, 'введите сообщение, no, если хотите отмену')
        bot.register_next_step_handler(message, send_all)
        
    if message.text == '/update7218':
        bot.send_message(message.chat.id, 'введите сообщение по спикерам, no, если хотите отмену')
        bot.register_next_step_handler(message,  update_all)

    if message.text == '/start':
        append_chat_id(message.chat.id)
        msg = welcome_msg
        bot.send_message(message.chat.id, msg, reply_markup = level1)
        msg = welcome_msg2
        bot.send_message(message.chat.id, msg, reply_markup = level1)
        doc = open('programm.pdf', 'rb')
        bot.send_document(message.chat.id, doc)

        msg = welcome_msg3
        photo = open('plan.jpg', 'rb')
        
        bot.send_message(message.chat.id, msg, reply_markup = level1)
        bot.send_photo(message.chat.id, photo)


        bot.register_next_step_handler(message, process_level2)
    



def send_all(message):
    try:
        if message.text != 'no': #### программа
            ids = get_ids()
            for id in ids:
                 bot.send_message(id, message.text)
                 
    
    except Exception as e:
        print (e)
        bot.reply_to(message, 'oooops')


def update_all(message):
    try:
        if message.text != 'no': #### программа
            update_current(message.text)
    except Exception as e:
        print (e)
        bot.reply_to(message, 'oooops')


def process_level2(message):
    try:
        if message.text == msg_l1[0]: #### программа
            msg = 'Выберите поток'
            bot.send_message(message.chat.id, msg, reply_markup = level2)
            bot.register_next_step_handler(message, process_level3)

        if message.text == msg_l1[1]: #### Кто сейчас выступает?
            msg = get_current()
            bot.send_message(message.chat.id, msg, reply_markup = level1)
            bot.register_next_step_handler(message, process_level2)

        if message.text == msg_l1[2]: ### получить программу
            msg = ''' Ссылка на программу 
                    https://ru.scribd.com/document/449221974/Программа-Get-Business-fest'''

            bot.send_message(message.chat.id, msg, reply_markup = level1)
            bot.register_next_step_handler(message, process_level2)

        if message.text == msg_l1[3]: 
            msg = ret()[-1]
            bot.send_message(message.chat.id, msg, reply_markup = level1)
            bot.register_next_step_handler(message, process_level2)

    except Exception as e:
        bot.reply_to(message, 'oooops')


def process_level3(message):
    try:
        if message.text == msg_l2[0]: ### Главная сцена
            msg = ret()[0]
            bot.send_message(message.chat.id, msg)
            bot.register_next_step_handler(message, process_level3)

        if message.text == msg_l2[1]: ### Главная сцена
            msg = ret()[1]
            bot.send_message(message.chat.id, msg)
            bot.register_next_step_handler(message, process_level3)
        
        if message.text == msg_l2[2]: ### Главная сцена
            msg = ret()[2]
            bot.send_message(message.chat.id, msg)
            bot.register_next_step_handler(message, process_level3)
        
        if message.text == msg_l2[3]: ### Главная сцена
            msg = ret()[3]
            bot.send_message(message.chat.id, msg)
            bot.register_next_step_handler(message, process_level3)

        if message.text == msg_l2[4]: ### Главная сцена
            msg = ret()[4]
            bot.send_message(message.chat.id, msg)
            bot.register_next_step_handler(message, process_level3)
        
        
        if message.text == msg_l2[-1]:
            msg = 'Возвращаемся в начало'
            bot.send_message(message.chat.id, msg, reply_markup = level1)
            bot.register_next_step_handler(message, process_level2)

    except Exception as e:
        bot.reply_to(message, 'oops, давайте попробуем еще раз')


# Handle all other messages with content_type 'text' (content_types defaults to ['text'])
@bot.message_handler(func=lambda message: True)
def echo_message(message):
    if message.text == 'Вернуться назад':
        msg = 'Возвращаемся в начало'
        bot.send_message(message.chat.id, msg, reply_markup = level1)
        bot.register_next_step_handler(message, process_level2)

bot.polling()
while True:
    pass